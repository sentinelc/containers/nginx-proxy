#!/usr/bin/env bash

# shellcheck disable=2016

envsubst < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf
