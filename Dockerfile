FROM docker.io/nginx:1.24.0-bullseye

COPY 99-generate-nginx-conf.sh /docker-entrypoint.d

COPY nginx.conf.template /etc/nginx/nginx.conf.template

EXPOSE 80
